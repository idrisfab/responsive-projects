#!/bin/bash

echo "Confirmation File, each ser in the document has had the key set" >> /root/keysConfirmation.txt 

for USER in $(cat /root/users.txt); do
    
    echo "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAgEAuV2Q6jJmw68Y5FBXEwKZ0CY8x30EtcGcSn6kbuIQqu8ymFrB4/Od/K3wOGygGlz86j5tIEfGdIp0wWoGD5P4ezg6plVaCdP6AmGeBjDqZs77IAQYk4+PsSw9HCwu5rC50y4TPefmA79Sj//fDF02IDKkNZxfDCZ8g+GdoSTxZM4qIyuYFpzachOvto2ZP79eQpV7Mzqia447UA3J6mBmSG+VeIklaEY8EgZqJqIGnpGb2bfZPkhpyiezV22JRVmhKjIVsXTF5JrtoX326xBjHj4AELHlotj3Lmf3WfvOOI+DgVVEzBlPMdve3pSflrRYqaTyowy185bQGRl2e3dT2drh1q55t43QRZ53yWMJo53peFeQ3h9/KZz7zd44eRCwIWMavCA4V1K5D5iqp34P6jRfq60tXnFOC+S2heTS8L9jBq7QA10iaST2zYO06eCkhipeloxpK0redK836FXEh6Gvn4VxK4Oqe5rRFB56i2BzRxM/0sjtBmaaPtTO1N5p/rbDPUupmGGWdnL4eKPJ9Ko76oH43fjBU0NZgPB/Jy2mchXY1lAzG/XcUMRb3dUReBAS1xKfvrknPC3pjBj3OwNrLk7BbVNqaFKtZnL/GQ74EaV3je5kHX9Cg9Cq3BlnDA1zXZFCA5QUigqcGk9jSY7gfNEub6jbykI9nN2azeE= gitlab-runner@idris-app-l4-vm" >> ~/.ssh/authorized_keys
    echo "$USER updated" >> /root/keysConfirmation.txt
done

echo "Complete successfully"  